import numpy as np

class Graph:
    def __init__(self):
        self.int_list = []
        self.int_dict = {}
        self.vertices = []
        self.int_mat = np.zeros((0,0))
        
    def get_list(self):
        return self.int_list
    
    def get_dict(self):
        return self.int_dict
    
    def get_mat(self):
        return self.int_mat
    
    def get_vertices(self):
        return self.vertices
    
    def set_list(self, list):
        self.int_list = list

    def set_dict(self, dict):
        self.int_dict = dict
    
    def set_mat(self, mat):
        self.int_mat = mat

    def convert_list_int_to_mat(self, list_int):
        '''

        This function allows to convert a list of interactions into a matrix of
        interactions.

        Parameters
        ----------
        list
            The list of interactions.

        Returns
        -------
        matrix
            The matrix of interactions.

        '''
        mat = np.zeros((len(self.get_vertices()), len(self.get_vertices())))
        for link in list_int:
            index0 = self.get_vertices().index(link[0])
            index1 = self.get_vertices().index(link[1])
            mat[index0][index1] = 1
            mat[index1][index0] = 1
        return mat
    
    def convert_list_int_to_dict(self, list_int):
        '''

        This function allows to convert a list of interactions into a
        dictionnary of interactions.

        Parameters
        ----------
        list
            The list of interactions.

        Returns
        -------
        dict
            The dictionnary of interactions.

        '''
        dict = {}
        for link in list_int:
            if link[0] not in dict:
                dict[link[0]] = {}
                dict[link[0]]["voisins"] = set()
                dict[link[0]]["voisins"].add(link[1])
            else:
                dict[link[0]]["voisins"].add(link[1])
            if link[1] not in dict:
                dict[link[1]] = {}
                dict[link[1]]["voisins"] = set()
                dict[link[1]]["voisins"].add(link[0])
            else:
                dict[link[1]]["voisins"].add(link[0])
        return dict
    
    def count_vertices(self):
        '''

        This function allows to return the number of vertices of a graph.

        Parameters
        ----------
        None.

        Returns
        -------
        int
            The number of vertices.

        '''
        return len(self.get_vertices())
    
    def count_edges(self):
        '''

        This function allows to return the number of edges of a graph.

        Parameters
        ----------
        None.

        Returns
        -------
        int
            The number of edges.

        '''
        return len(self.get_list())
    
    def get_degree(self, vertex):
        '''

        This function allows to return the degree of a given protein
        of a graph.

        Parameters
        ----------
        string
            The name of the protein.

        Returns
        -------
        int
            The degree of the given protein.

        '''
        return len(self.get_dict()[vertex]["voisins"])

    def get_max_degree(self):
        '''

        This function allows to return the protein with the highest degree and
        the degree.

        Parameters
        ----------
        None.

        Returns
        -------
        string
            The name of the protein.
        int
            The degree associated.

        '''
        deg_max = 0
        vertex_max = 0
        for vertex in self.get_dict().keys():
            if self.get_degree(vertex) > deg_max:
                deg_max = self.get_degree(vertex)
                vertex_max = vertex
        return vertex_max, deg_max
    
    def get_ave_degree(self):
        '''

        This function allows to return the average degree of proteins
        of the graph.

        Parameters
        ----------
        None.

        Returns
        -------
        float
            The average degree of the graph.

        '''
        deg_sum = 0
        for vertex in self.get_dict().keys():
            deg_sum += self.get_degree(vertex)
        return deg_sum / self.count_vertices()
    
    def get_sum_degree(self):
        '''

        This function allows to return the sum of the degrees of the graph.

        Parameters
        ----------
        None

        Return
        -------
        None.
        '''
        sum_deg = 0
        for vertex in self.get_vertices():
            sum_deg += self.get_degree(vertex)
        return sum_deg
    
    def count_degree(self, deg):
        '''

        This function allows to return the number of proteins with a given
        degree.

        Parameters
        ----------
        int
            A degree.

        Returns
        -------
        int
            The number of proteins with the given degree.

        '''
        nb_vertex = 0
        for vertex in self.get_dict().keys():
            if self.get_degree(vertex) == deg:
                nb_vertex += 1
        return nb_vertex
    
    def histogram_degree(self, dmin, dmax):
        '''

        This function allows to print the histogram of degrees of the graph.

        Parameters
        ----------
        int
            The minimum degree.
        int
            The maximum degree.        

        Returns
        -------
        None

        '''
        for i in range(dmin, dmax + 1):
            degree_count = self.count_degree(i)
            histogram_line = str(i) + " " + "*" * degree_count
            print(histogram_line)

    def density(self):
        '''

        This function allows to return the density of the graph.

        Parameters
        ----------
        None

        Returns
        -------
        float
            The density of the graph.

        '''
        return (2 * self.count_edges()) / (self.count_vertices() * (self.count_vertices() - 1))
    
    def clustering(self, vertex):
        '''

        This function allows to return the coefficient of clustering of a
        protein given in parameter.

        Parameters
        ----------
        string 
            The name of the protein.

        Returns
        -------
        float
            The coefficient of clustering of the protein.

        '''
        denom = self.get_degree(vertex) * (self.get_degree(vertex) - 1)
        neigh = self.get_dict()[vertex]
        nume = 0
        for link in self.get_list():
            if link[0] in neigh:
                if link[1] in neigh:
                    nume += 1
        return 2 * nume / denom
    
    def extract_CC(self, start):
        '''

        This function allows to return the proteins of a connexe component.

        Parameters
        ----------
        string
            The protein to start the search of the connexe component.

        Returns
        -------
        list
            The list of proteins of the connexe component.

        '''
        vertex_visited = set()
        vertex_unvisited = [start]
        while len(vertex_unvisited) != 0:
            vertex = vertex_unvisited.pop()
            if vertex not in vertex_visited:
                vertex_visited.add(vertex)
                vertex_unvisited.extend(self.get_dict()[vertex]["voisins"] - vertex_visited)
        return vertex_visited

    def count_CC(self):
        '''

        This function allows to return the number of connexe components and
        their respective sizes.

        Parameters
        ----------
        None

        Returns
        -------
        int
            The number of connexe components.
        list
            The size of each component.

        '''
        vertex = self.get_vertices()[0]
        compound_list = [self.extract_CC(vertex)]
        for new_vertex in self.get_vertices():
            deja_presente = False
            for compound in compound_list:
                if new_vertex in compound:
                    deja_presente = True
            if not deja_presente:
                compound_list.append(self.extract_CC(new_vertex))
        return len(compound_list), [(len(compound), compound) for compound in compound_list]
    
    def write_CC(self, file_out):
        '''

        This function allows to create a file containing the connexe
        components. The first column is the size of the component and the
        second column is the list of proteins composing the component.

        Parameters
        ----------
        string
            The name of the outfile.

        Returns
        -------
        None

        '''
        lignes = []
        nombre_composantes, info_composantes = self.count_CC()
        for index in range(nombre_composantes):
            ligne = ""
            taille, composante = info_composantes[index]
            ligne += str(taille)
            ligne += "\t"
            ligne += ",".join(composante)
            ligne +="\n"
            lignes.append(ligne)
        with open(file_out, "w") as fh:
            fh.writelines(lignes)
    
    def compute_CC(self):
        '''

        This function allows to return a list, each element of which
        corresponds to the number of the protein's related component in the
        list of proteins in the graph.

        Parameters
        ----------
        None

        Returns
        -------
        list
            The list corresponding to the numbers of connexe component.

        '''
        vertices = self.get_vertices()
        lcc = []
        nombre_composantes, info_composantes = self.count_CC()
        for protein in vertices:
            for index in range(nombre_composantes):
                composante = info_composantes[index][1]
                if protein in composante:
                    lcc.append(index)
                    break
            else: 
                raise Exception("protein not found in any compound")
        return lcc