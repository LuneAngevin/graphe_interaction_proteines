import re
import os
import numpy as np
import random
import copy
from urllib.request import urlopen
import tempfile

from proteome import Proteome
from domaingraph import DomainGraph
from graph import Graph

class Interactome(Graph):
    def __init__(self):
        super().__init__()
        self.random_graph_er = None
        self.random_graph_ba = None
        
    def read_file(self, file, proteome: Proteome):
        '''

        This function allows to initialize the attributs of the interactome
        according to an interaction file found in parameter.

        Parameters
        ----------
        string
            The name of the file describing the graph.

        Returns
        -------
        None

        '''
        dict_graph = {}
        list_graph = []
        vertices_seen = []
        with open(file, "r") as fh:
            lines = fh.readlines()
            for line in lines[1:]:
                vertices = line.strip().split("\t")
                list_graph.append((vertices[0], vertices[1]))
                for vertice in vertices:
                    if vertice not in vertices_seen:
                        vertices_seen.append(vertice)
                vertices_seen.sort()
                if vertices[0] not in dict_graph:
                    dict_graph[vertices[0]] = {}
                    dict_graph[vertices[0]]["voisins"] = set()
                    dict_graph[vertices[0]]["voisins"].add(vertices[1])
                else:
                    dict_graph[vertices[0]]["voisins"].add(vertices[1])
                if vertices[1] not in dict_graph:
                    dict_graph[vertices[1]] = {}
                    dict_graph[vertices[1]]["voisins"] = set()
                    dict_graph[vertices[1]]["voisins"].add(vertices[0])
                else:
                    dict_graph[vertices[1]]["voisins"].add(vertices[0])
            matrix = np.zeros((len(vertices_seen), len(vertices_seen)))
            for line in lines[1:]:
                vertices = line.strip().split("\t")
                index0 = vertices_seen.index(vertices[0])
                index1 = vertices_seen.index(vertices[1])
                matrix[index0][index1] = 1
                matrix[index1][index0] = 1
        self.int_list = list_graph
        self.int_dict = dict_graph
        self.vertices = vertices_seen
        self.int_mat = matrix
        self.xlink_domains(proteome)
    
    def get_proteins(self):
        return self.vertices

    def clean_interactome(self, fileout):
        '''

        This function allows to return a clean interaction file from a first
        interaction file.

        Parameters
        ----------
        string 
            The name of the file to clean.
        string
            The name of the cleaned outfile.

        Returns
        -------
        None

        '''
        prot_list = self.get_proteins()
        mat = self.get_mat()
        for i in range(len(prot_list)):
            mat[i][i] = 0
        with open(fileout, "w") as fh:
            fh.write(str(int(np.sum(mat) / 2)))
            for i in range(len(prot_list)):
                for j in range(i + 1, len(prot_list)):
                    if mat[i][j] == 1:
                        fh.write("\n" + str(prot_list[i]) + "\t" + prot_list[j])
    
    def graph_er(self, p):
        '''

        This function allows to create a random graph of Erdös-Renyi.

        Parameters
        ----------
        float
            The probability of creating an edge between two vertices.

        Returns
        -------
        None

        '''      
        random_graph_er = Interactome()                    
        list_int = []
        for i in range(len(self.get_proteins())):
            for j in range(i+1, len(self.get_proteins())):
                r = random.random()
                if r <= p:
                    list_int.append((self.get_proteins()[i], self.get_proteins()[j]))
        random_graph_er.set_list(list_int)
        random_graph_er.set_mat(self.convert_list_int_to_mat(list_int))
        random_graph_er.set_dict(self.convert_list_int_to_dict(list_int))
        self.random_graph_er = random_graph_er

    def graphe_ba(self, nb_vertices = 2):
        '''

        This function allows to create a random graph of Barabasi-Albert.

        Parameters
        ----------
        int
            The number of vertices to initialize the graph.

        Returns
        -------
        None

        '''
        random_graph_ba = Interactome()
        list_int = []
        new_list_prot = []
        first_list_prot = copy.deepcopy(self.get_proteins())
        new_list_prot.extend(np.random.choice(first_list_prot, nb_vertices, replace = False))
        for prot in new_list_prot:
            first_list_prot.remove(prot)
        for i in range(len(new_list_prot)):
            for j in range(i+1, len(new_list_prot)):
                list_int.append((new_list_prot[i], new_list_prot[j]))
        for new_prot in first_list_prot:
            for prot in new_list_prot:
                p = self.get_degree(prot) / self.get_sum_degree()
                r = random.random()
                if r <= p:
                    list_int.append((new_prot, prot))
            new_list_prot.append(new_prot)
        random_graph_ba.set_list(list_int)
        random_graph_ba.set_mat(self.convert_list_int_to_mat(list_int))
        random_graph_ba.set_dict(self.convert_list_int_to_dict(list_int))
        self.random_graph_ba = random_graph_ba
    
    def xlink_uniprot(self, proteome: Proteome):
        '''

        This function allows to return a new dictionnary wich give for each
        protein the neighbors and the uniprot ID.

        Parameters
        ----------
        Proteome
            The proteome corresponding to the proteins of the graph.

        Returns
        -------
        dict
            The new dictionnary.

        '''
        new_dict = {}
        for prot in self.get_dict():
            new_dict[prot] = self.get_dict()[prot]
            try:
                new_dict[prot]["UniprotID"] = proteome.get_dict_name_to_ID()[prot]
            except KeyError as key_error:
                new_dict[prot]["UniprotID"] = None
        return new_dict
    
    def get_protein_domains(self, p):
        '''

        This function allows to return a list corresponding to the domains
        composing a protein, wich Uniprot ID is given in parameter.

        Parameters
        ----------
        string
            The UniprotID of the protein.

        Returns
        -------
        list
            The list corresponding to the domains of the protein.

        '''

        if os.path.isfile(f"/home/bioinfo/projet/cache/{p}"):
            with open(f"/home/bioinfo/projet/cache/{p}", "r") as f:
                infos = f.read()
        else:
            url = f"https://rest.uniprot.org/uniprotkb/{p}.txt"
            infos = urlopen(url).read().decode()
            with open(f"/home/bioinfo/projet/cache/{p}", "w") as f:
                f.write(infos)
        regex = re.compile(r"FT\s+DOMAIN\s+\d+\.\.\d+\nFT\s+\/note=\"([a-zA-Z -]+[a-zA-Z-])\s?\d*\"")
        res = regex.findall(infos)
        return res
        
    def xlink_domains(self, p):
        '''

        This function allows to modify the dictionnary, adding the composition
        in domains of each protein.

        Parameters
        ----------
        Proteome
            The proteome corresponding to the proteins of the graph.

        Returns
        -------
        None

        '''
        print("[+] linking domains")
        new_dict = self.xlink_uniprot(p)
        for prot in new_dict:
            id = new_dict[prot]["UniprotID"]
            if id is None:
                new_dict[prot]["domains"] = None
            else:
                new_dict[prot]["domains"] = self.get_protein_domains(id)
        self.set_dict(new_dict)

    def ls_domains(self):
        '''

        This function allows to return the list of all the domains composing
        the proteins of the graph.

        Parameters
        ----------
        None

        Returns
        -------
        list
            The list of the domains.

        '''
        domains_list = set()
        for prot in self.get_dict():
            if self.get_dict()[prot]["domains"] is not None:
                for domain in self.get_dict()[prot]["domains"]:
                    if domain not in domains_list:
                        domains_list.add(domain)
        return domains_list
    
    def ls_domains_n(self, n):
        '''

        This function allows to return the list the domains composing the
        proteins that we see more than n times.

        Parameters
        ----------
        int
            The minimum number of times we want to see the domains.

        Returns
        -------
        list
            The list of the domains  present at least n times.

        '''
        domains_list_red = []
        domains_list_nonred = set ()
        for prot in self.get_dict():
            for domain in self.get_dict()[prot]["domains"]:
                domains_list_red.append(domain)
        for domain in domains_list_red:
            if domains_list_red.count(domain) >= n:
                domains_list_nonred.add(domain)
        return domains_list_nonred

    def nbDomainsByProteinDistribution(self):
        '''

        This function allows to print the distribution of the number of domains
        by protein.

        Parameters
        ----------
        None

        Returns
        -------
        None

        '''
        dict_nb_domains = {}
        for prot in self.get_dict():
            nb_domains = len(self.get_dict()[prot]["domains"])
            if nb_domains not in dict_nb_domains:
                dict_nb_domains[nb_domains] = 1
            else:
                dict_nb_domains[nb_domains] += 1
        sorted_keys = sorted(dict_nb_domains.keys())
        for i in sorted_keys:
            histogram_line = str(dict_nb_domains[i]) + " " + "*" * i
            print(histogram_line)

    def co_occurrence(self, dom_x, dom_y):
        '''

        This function allows to return the number of co-occurrence of two
        domains given in parameter. If a domain is present several times in a
        protein, it will only count for one time.

        Parameters
        ----------
        string
            The domain 1.
        string
            The domain 2.

        Returns
        -------
        int
            The number of co-occurrences.

        '''
        nb_co_occ = 0
        dict = self.get_dict()
        for prot in dict:
            if dict[prot]["domains"] is not None:
                if dom_x in dict[prot]["domains"] and dom_y in dict[prot]["domains"]:
                    nb_co_occ += 1
        return nb_co_occ
    
    def co_occurrence_2(self, dom_x, dom_y):
        '''

        This function allows to return the number of co-occurrence of two
        domains given in parameter. If a domain is present several times in a
        protein, it will be taken into account.

        Parameters
        ----------
        string
            The domain 1.
        string
            The domain 2.

        Returns
        -------
        int
            The number of co-occurrences.

        '''
        nb_co_occ = 0
        dict = self.get_dict()
        for prot in dict:
            if dict[prot]["domains"] is not None:
                for domain1 in dict[prot]["domains"]:
                    if domain1 == dom_x:
                        for domain2 in dict[prot]["domains"]:
                            if domain2 == dom_y:
                                nb_co_occ += 1
        return nb_co_occ

    def generate_cooccurrence_graph(self):
        '''

        This function allows to generate a co-occurrence domain graph according
        to the co-occurrence of the domains.

        Parameters
        ----------
        None.

        Returns
        -------
        None.

        '''
        print("[...] generating domain graph")
        list_domains = list(self.ls_domains())
        list_int = []
        for i in range (0,len(list_domains)):
            for j in range (i+1, len(list_domains)):
                if self.co_occurrence(list_domains[i], list_domains[j]) > 0:
                    list_int.append((list_domains[i], list_domains[j]))
        domain_graph = DomainGraph(list_domains, list_int)
        return domain_graph

    def most_common_domains(self):
        '''

        This function allows to return the list of the 10 most common domains
        found in proteins.

        Parameters
        ----------
        None.

        Returns
        -------
        list
            The 10 most common domains.

        '''
        def get_nb_occ(duo):
            return duo[1]
        domain_dict = {}
        prot_dict = self.get_dict()
        for prot in prot_dict:
            if prot_dict[prot]["domains"] is not None:
                for domain in prot_dict[prot]["domains"]:
                    if domain not in domain_dict:
                        domain_dict[domain] = 1
                    else:
                        domain_dict[domain] += 1
        list_domains = list(domain_dict.items())
        list_sorted_domains = sorted(list_domains, key = get_nb_occ, reverse = True)
        return list_sorted_domains[:10]

    def domain_co_occ_themselves(self):
        '''

        This function allows to return the number of domains that have a co-
        occurrence with themselves.

        Parameters
        ----------
        None.

        Returns
        -------
        int
            The number of domains in co-occurrence with themselves.

        '''
        nb_domains = 0
        for domain in self.ls_domains():
            if self.co_occurrence(domain, domain) > 0:
                nb_domains += 1
        return nb_domains
    
    def generate_cooccurrence_graph_np(self, n):
        '''

        This function allows to generate a co-occurrence domain graph according
        to the co-occurrence of the domains with a threshold.

        Parameters
        ----------
        int
            The threshold to create an interaction between two domains.

        Returns
        -------
        None.

        '''
        print("[...] generating domain graph")
        list_domains = list(self.ls_domains())
        list_int = []
        for i in range (0,len(list_domains)):
            for j in range (i+1, len(list_domains)):
                if self.co_occurrence(list_domains[i], list_domains[j]) >= n:
                    list_int.append((list_domains[i], list_domains[j]))
        domain_graph = DomainGraph(list_domains, list_int)
        return domain_graph
    
    def generate_cooccurrence_graph_n(self, n):
        '''

        This function allows to generate a co-occurrence domain graph according
        to the co-occurrence of the domains with a threshold.

        Parameters
        ----------
        int
            The threshold to create an interaction between two domains.

        Returns
        -------
        None.

        '''
        print("[...] generating domain graph")
        list_domains = list(self.ls_domains())
        list_int = []
        for i in range (0,len(list_domains)):
            for j in range (i+1, len(list_domains)):
                if self.co_occurrence_2(list_domains[i], list_domains[j]) >= n:
                    list_int.append((list_domains[i], list_domains[j]))
        domain_graph = DomainGraph(list_domains, list_int)
        return domain_graph

    def weighted_cooccurrence_graph(self):
        '''

        This function allows to generate a co-occurrence domain graph according
        to the co-occurrence of the domains with weights corresponding to the  
        numbre of co-occurrences.

        Parameters
        ----------
        None.

        Returns
        -------
        None.

        '''
        print("[...] generating weighted domain graph")
        list_domains = list(self.ls_domains())
        list_int = []
        for i in range (0,len(list_domains)):
            for j in range (i+1, len(list_domains)):
                nb_co_occ = self.co_occurrence_2(list_domains[i], list_domains[j])
                if nb_co_occ > 0:
                    list_int.append((list_domains[i], list_domains[j], nb_co_occ))
        domain_graph = DomainGraph(list_domains, list_int)
        return domain_graph