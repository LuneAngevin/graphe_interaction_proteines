from Bio import SeqIO

class Proteome:
    def __init__(self, file):
        list_prot = []
        dict_name_to_ID = {}
        dict_ID_to_name = {}
        with open(file, "r") as fasta_file:
            for record in SeqIO.parse(fasta_file, "fasta"):
                id = record.name.split("|")[1]
                name = record.name.split("|")[2]
                list_prot.append(name)
                dict_name_to_ID[name] = id
                dict_ID_to_name[id] = name
        
        self.list_prot = list_prot
        self.dict_name_to_ID = dict_name_to_ID
        self.dict_ID_to_name = dict_ID_to_name

    def get_dict_name_to_ID(self):
        return self.dict_name_to_ID  