from graph import Graph

class DomainGraph(Graph):
    
    def __init__(self,domains_list, int_list):
        super().__init__()
        domains_dict = {}
        for domain in domains_list:
            domains_dict[domain] = {}
            domains_dict[domain]["voisins"] = set()
        for link in int_list:
            domains_dict[link[0]]["voisins"].add(link[1])
            domains_dict[link[1]]["voisins"].add(link[0])
        self.vertices = domains_list
        self.int_list = int_list
        self.int_dict = domains_dict

    def get_domains(self):
        return self.vertices

    def get_10_max_degree(self):
        '''

        This function allows to return the 10 domains with the maximum degrees
        and their associated degrees.

        Parameters
        ----------
        None.

        Returns
        -------
        list
            The 10 domains of maximum degrees and their degrees.
        '''
        print("[...] calculating max degree")
        def get_degree_from_duo(duo):
            return duo[1]
        list_degree = []
        for domain in self.get_domains():
            list_degree.append((domain,self.get_degree(domain)))
        sorted_list_degree = sorted(list_degree, key=get_degree_from_duo, reverse=True)
        return sorted_list_degree[:10]
    
    def get_10_min_degree(self):
        '''

        This function allows to return the 10 domains with the minimum degrees
        and their associated degrees.

        Parameters
        ----------
        None.

        Returns
        -------
        list
            The 10 domains of minimum degrees and their degrees.
        '''
        print("[...] calculating min degree")
        def get_degree_from_duo(duo):
            return duo[1]
        list_degree = []
        for domain in self.get_domains():
            list_degree.append((domain,self.get_degree(domain)))
        sorted_list_degree = sorted(list_degree, key=get_degree_from_duo)
        return sorted_list_degree[:10]
    
    def write_domains_file(self, outfile):
        '''

        This function allows to create a file containing the domain graph.
        Each line represents an interaction between two domains.

        Parameters
        ----------
        string
            The name of the outfile.

        Returns
        -------
        None

        '''
        print("[...] writing domains file")
        with open(outfile, 'w') as fh:
            for interaction in self.get_list():
                fh.write('{}\t{}\n'.format(interaction[0], interaction[1]))

    def graph_threshold(self, k):
        '''

        This function allows to modify the domain graph according to a
        threshold for the interactions.

        Parameters
        ----------
        int
            The threshold to keep an interaction.

        Returns
        -------
        None

        '''
        new_int_list = []
        for interaction in self.get_list():
            if interaction[2] >= k:
                new_int_list.append(interaction)
        self.set_list(new_int_list)